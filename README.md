# Feedly - Open all unread in new tabs

This is a chrome extension. When using Feedly, you will be able to open all unread articles by clicking the Tab button.

## Installation:
1. Make sure you enable developer mode on chrome, to load extensions not originating from the app store.
2. Clone or download this repo, so that the files are present locally on your computer.
3. Load an unpacked extension onto chrome. 

## Usage:
To use the extention, navigate to any Feedly feed where there are unread articles. From there, click the "Tab" button located in the top right- to the left of the checkmark.
![Screenshot of Extension in Action](/demo.png "Extension In Action")
