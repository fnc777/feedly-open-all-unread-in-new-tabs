window.onload = function(){
	console.log('Background JS active..');

	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		if(request.action === 'listArticles'){
			var listLinks = request.articles;
			console.log('Opening ' + listLinks.length + ' articles.');
			listLinks.forEach(function(element){
				chrome.tabs.create({
					"windowId" : sender.tab.windowId,
					"url" : element,
					"active" : false
				});
			});
		}
	});
};