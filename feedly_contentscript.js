(function(){
	console.log("Feedly tab opener init.");

	jQuery(document).ready(function(){
		var timer = setInterval(function (){
			// Inject the new tab button only once the page has completed loading.
			if (document.querySelector('#pageActionMarkAsRead') !== null){
				// Append onto action layouts the execution button
				console.log('Adding extension button');
				var imgUrl = chrome.extension.getURL("newtab.png");
				var button = jQuery("<img>")
				.attr({
					"src" : imgUrl,
					"id" : "feedly_open_all_unread_in_tabs_button",
					"class" : "pageAction",
					"height" : "24px",
					"width" : "51px"
				})
				.prependTo(".pageActionBar")
				.click(function(){
					execute();
				});
				clearTimeout(timer);
			}
		}, 100); // Poll every 100 ms, to see if the page has completed loading.
	});

	function openUnread(){
		// Collect all of the links
		var jqArrayLinks = jQuery("#timeline .unread").map(function(index, element){
			return element.href;
		});
		var jsArrayLinks = jQuery.makeArray(jqArrayLinks);
		var message = {
			"action" : "listArticles",
			"articles" : jsArrayLinks
		};
		console.log("Opening " + jsArrayLinks.length + " links.");
		chrome.runtime.sendMessage(message);
	}

	function execute(){
		// Find the number of unread articles
		// Determine if we're in a feed or category
		var unreadCount;
		if (location.href.search("subscription") > -1){
			var feedUrl = unescape(location.href) // Get the feed url contained in the url
			.split("subscription")[1] // The feed url is separated by /subscription/
			.substring(1); // Remove the '/' preceeding the feed url

			jQuery("div .feedUnreadCount").each(function(index, element){
				if (jQuery(this).attr("data-feedid") === feedUrl){
					unreadCount = parseInt(jQuery(this).text(), 10);
				}
			});
		} else {
			// We're inside a category
			// Determine if we're on the front page vs. a specific category.
			var dataCategory;
			if (location.href.search("#latest") > -1){
				dataCategory = "#latest";
			} else {
				dataCategory = unescape(location.href) // Get the feed url contained in the url
				.split("category")[1] // The feed url is separated by /subscription/
				.substring(1); // Remove the '/' preceeding the feed url
			}

			jQuery("div .categoryUnreadCount").each(function(index, element){
				if (jQuery(this).attr("data-category") === dataCategory){
					unreadCount = parseInt(jQuery(this).text(), 10);
				}
			});
		}

		var timelineCount = jQuery("#timeline .title").length;

		// See if all of the unread articles are already present
		if (unreadCount === jQuery("#timeline .unread").length){
			openUnread();
			return;
		}

		// Scroll down all the way
		// To expose all of the unread articles.
		window.scrollTo(0, document.body.scrollHeight);

		// Wait a few milliseconds for Feedly to load the new articles if they are present.
		var timer = setInterval(function(){
			if (timelineCount !== jQuery("#timeline .title").length || document.querySelector("#bigMarkAllAsReadButton") !== null){
				// When there is no unread artcles to retrieve, stop all timers.
				if (unreadCount === jQuery("#timeline .unread").length){
					clearTimeout(timer);
					// Scroll to the top.
					window.scrollTo(0,0);
					// Execute the remainder of the code.
					openUnread();
				} else {
					// Update count and keep scrolling down.
					timelineCount = jQuery("#timeline .title").length;
					window.scrollTo(0, document.body.scrollHeight);
				}
			}
		}, 100);
	}
})();